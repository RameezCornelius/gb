/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SeleniumPackages;

/**
 *
 * @author Lenovo G50
 */
public class SeleniumPageObjects {

    public static String JPMorganURL() {
        return " https://am.jpmorgan.com/gb/en/asset-management/gim/adv/home";
    }

    public static String AcceptDisclaimer() {
        return "//div[@class='btn btn-cta ng-binding']";
    }

    public static String FindAFund() {
        return "//a[@title='Find a fund']";
    }


    public static String FilterBySearchBar() {
        return "//input[@id='fundNameFilter']";
    }

    public static String FundsSearchBar() {
        return "//input[@type='search']";
    }

    public static String ShareClassesAmount() {
        return "//span[contains(text(),'Available shareclasses')]";
    }
    
    public static String FirstAvaibleFund() {
        return "//a[@data-testid='fund-name'][1]";
    }


    public static String PerformanceAndFeesTab() {
        return "//a[@data-tab-name='performance']";
    }

    public static String PerformanceChart() {
        return "//div[@id='PerformanceChart']";
    }

}
