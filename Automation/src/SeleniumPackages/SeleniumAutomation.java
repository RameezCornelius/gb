/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SeleniumPackages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


/**
 *
 * 
 */
public class SeleniumAutomation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        ////initialize driver
        WebDriver driver = new ChromeDriver();
        
        try {
            
            //Navigate to JP MorgonURL
            driver.get(SeleniumPageObjects.JPMorganURL());
            
            //Wait for 'Accept Disclaimer' window
            if (!SeleniumUtility.waitForElementbyXpath(driver, SeleniumPageObjects.AcceptDisclaimer())) {
                throw new Exception("Failed to wait for 'Accept Disclaimer'");
            }
            //Click the 'Accept Disclaimer' button
            if (!SeleniumUtility.clickElementByXpath(driver, SeleniumPageObjects.AcceptDisclaimer())) {
                throw new Exception("Failed to click 'Accept Disclaimer'");
            }
             
            //Wait for 'Find a Fund' quick link
            if (!SeleniumUtility.waitForElementbyXpath(driver, SeleniumPageObjects.FindAFund())) {
                throw new Exception("Failed to wait for 'Find a Fund' quick link");
            }
            //Click 'Find a Fund' quick link
            if (!SeleniumUtility.clickElementByXpath(driver, SeleniumPageObjects.FindAFund())) {
                throw new Exception("Failed to click the 'Find a Fund' quick link");
            }
            
            //Wait for search bar to render
            if (!SeleniumUtility.waitForElementbyXpath(driver, SeleniumPageObjects.FilterBySearchBar())) {
                throw new Exception("Failed to wait for Search Bar");
            }
            
            //Wait for results to be displayed before filtering list
            String numberOfResultsBeforeFilter = driver.findElement(By.xpath(SeleniumPageObjects.ShareClassesAmount())).getText();
            
            //Enter text into search bar 
            driver.findElement(By.xpath(SeleniumPageObjects.FilterBySearchBar())).sendKeys("JPM");
            
            //Check that list automatically filters and the amount of results has changed
            String numberOfResultsAfterFilter = driver.findElement(By.xpath(SeleniumPageObjects.ShareClassesAmount())).getText();
            
            //Wait for first fund to load
            if (!SeleniumUtility.waitForElementbyXpath(driver, SeleniumPageObjects.FirstAvaibleFund())) {
                throw new Exception("Failed to wait for first available fund");
            }
            
            //Click first fund
            if (!SeleniumUtility.clickElementByXpath(driver, SeleniumPageObjects.FirstAvaibleFund())) {
                throw new Exception("Failed to click first available fund");
            }
            
            //Wait for 'Performance & Fees' tab to laod
            if (!SeleniumUtility.waitForElementbyXpath(driver, SeleniumPageObjects.PerformanceAndFeesTab())) {
                throw new Exception("Failed to wait for Performance & Fees");
            }
            
            //VWait for 'Performance & Fees' tab
            if (!SeleniumUtility.waitForElementbyXpath(driver, SeleniumPageObjects.PerformanceAndFeesTab())) {
                throw new Exception("Failed to wait for for Performance & Fees tab");
            }
            
            //Click 'Performance & Fees' tab
            if (!SeleniumUtility.clickElementByXpath(driver, SeleniumPageObjects.PerformanceAndFeesTab())) {
                throw new Exception("Failed to click Performance & Fees tab");
            }
 
            //Validate that for 'Performance & Fees' chart exists
            if (!SeleniumUtility.waitForElementbyXpath(driver, SeleniumPageObjects.PerformanceChart())) {
                throw new Exception("Failed to wait for for Performance & Fees chart");
            }
            
            //Close web driver
            driver.quit();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            driver.quit();
        }
    }

}
