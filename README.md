## The Following Repository is split into 2 sections:

1.Automation : 	-Selenium Automation 

2.Automation : 	-Appium Automation

3.API Test : 	-Postman Collection

---

## 1.Automation : Selenium Auotmation


1. Clone the project folder to a preferred location
2. Open the ''project folder using Netbeans IDE
3. Navigate to 'SeleniumPackages' and view the 'SeleniumAutomation.java' file within the source packages
4. Hit F5 within the file and watch the script execute

---

## 2.Automation : Appium Automation - INCOMPLETE


1. Assuming you already have the repo cloned, open the project folder using Netbeans IDE
2. Navigate to 'AppiumPackages' and view the 'AppiumAutomation.java' file within the source packages
3. Hit F5 within the file and watch the script execute

---

## 3.API Test : Postman Collection

1. Assuming you already have the repo cloned, open the 'API Test' folder 
2. Open the 'Test .postman_collection.json' collection within PostMan
3. Once the request has successfully imported, hit 'Send' and validate for a successful response